package io.gitee.znnspace.ocr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OcrExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(OcrExampleApplication.class, args);
	}
}
